import Button from './button';

export default {
  title: 'Button',
  component: Button,
};

export const Primary = () => <Button buttonType="primary">Primary</Button>;
