import './button.css';

enum buttonTypes {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  SUCCESS = 'success',
  DANGER = 'danger',
}

interface IButtonProps {
  buttonType?: string;
  children?: string;
}

function Button(props: IButtonProps) {
  const {
    buttonType = buttonTypes.PRIMARY,
    children = buttonTypes.PRIMARY,
    ...rest
  } = props;
  return (
    <button className={`button ${buttonType}`} {...rest}>
      {children}
    </button>
  );
}

export default Button;
