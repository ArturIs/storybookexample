import React from 'react';
import Button from './components/button/button';

function App() {
  return (
    <main>
      <Button />
    </main>
  );
}

export default App;
